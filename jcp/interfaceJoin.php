<?php

if (isset($_POST['submitForm'])) {

  function get_data($url)
  {
    $encode_resume = '';
    if($_FILES['MxDOTalentNetworkMemberInfo_ResumeWordDoc_FileName']['name'] != '')
    {
      $target_path = "uploads/";
      $target_path = $target_path . basename( $_FILES['MxDOTalentNetworkMemberInfo_ResumeWordDoc_FileName']['name']);

      if(move_uploaded_file($_FILES['MxDOTalentNetworkMemberInfo_ResumeWordDoc_FileName']['tmp_name'], $target_path)) {
        $resume = file_get_contents($target_path);
        $encode_resume = base64_encode($resume);
        unlink($target_path);
      } else{
          echo "There was an error uploading the file, please try again!";
      }
    }
    //super duper post variables and subsequent xml
    $email = $_POST["MxDOTalentNetworkMemberInfo_EmailAddress"];
    $fname = $_POST["MxDOTalentNetworkMemberInfo_FirstName"];
    $lname = $_POST["MxDOTalentNetworkMemberInfo_LastName"];
    $country = $_POST["ddlCountries"];
    $city = $_POST["txtCity"];
    $zip = $_POST["MxDOTalentNetworkMemberInfo_ZipCode"];
    $interest = '';//$_POST["JQ814G7754L8T8FV5ZVT"];
    $jobtitle = '';//$_POST["JQ812586G81JHC37PKF4"];

    $xml = "<Request>
    <DeveloperKey>WDT90JC6TPRV1C0VBQ4S</DeveloperKey>
    <TalentNetworkDID>TN815F26NX8XWJZ06XJ2</TalentNetworkDID>
    <PreferredLanguage>USEnglish</PreferredLanguage>
    <JoinPath></JoinPath>
    <ResumeWordDoc>$encode_resume</ResumeWordDoc>
    <JoinValues>
      <JoinValue>
        <Key>MxDOTalentNetworkMemberInfo_EmailAddress</Key>
        <Value>$email</Value>
      </JoinValue>
      <JoinValue>
        <Key>MxDOTalentNetworkMemberInfo_FirstName</Key>
        <Value>$fname</Value>
      </JoinValue>
      <JoinValue>
        <Key>MxDOTalentNetworkMemberInfo_LastName</Key>
        <Value>$lname</Value>
      </JoinValue>
      <JoinValue>
        <Key>ddlCountries</Key>
        <Value>$country</Value>
      </JoinValue>
      <JoinValue>
        <Key>txtCity</Key>
        <Value>$city</Value>
      </JoinValue>
      <JoinValue>
        <Key>MxDOTalentNetworkMemberInfo_ZipCode</Key>
        <Value>$zip</Value>
      </JoinValue>
      <JoinValue>
        <Key>JQ814G7754L8T8FV5ZVT</Key>
        <Value>$interest</Value>
      </JoinValue>
      <JoinValue>
        <Key>JQ812586G81JHC37PKF4</Key>
        <Value>$jobtitle</Value>
      </JoinValue>
    </JoinValues>
    </Request>";

    echo "<script>alert($xml)</script>";

    $headers = array(
      "Content-type: text/xml",
      "Content-length: " . strlen($xml),
      "Connection: close",
    );

    $ch = curl_init();
    $timeout = 5;
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_exec($ch);
    curl_close($ch);
    // return $data;

    echo "<script>alert('Thanks for joining our Talent Network!');
    location.href='./index.php';</script>";
  }

  $url = sprintf('https://api.careerbuilder.com/talentnetwork/member/create');
  echo get_data($url);

} else {
  echo "A form submission needs to occur. Please head back to the <a href='./join.php'>Join</a> page.";
}
?>
