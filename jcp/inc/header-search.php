    <aside class="header-search">
      <div class="constrained">
          <form name="job-search" action="jobs.php" method="get">
            <div class="search-fields">
	            <input type="text" name="keywords" id="keywords" placeholder="keywords" />
	            <input type="text" name="loc" id="location" placeholder="location" />
            </div>
            <a href="#" id="job-search-btn" class="large-btn">Search</a>
          </form>
      </div>
    </aside>