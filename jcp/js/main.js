/**
 * @fileOverview Bootstraps the site, handles UI functionality
 * @author Tim Snyder <tim@timsnyder.me>
 */

$(document).foundation();

(function($, window, document){
	"use strict";
	
	var search_btn = document.querySelector("#job-search-btn");
	var keywords_field = document.querySelector('#keywords');
	var location_field = document.querySelector('#location');
	var vid = document.querySelector("#modal-link");
	var media;
	
	function submitSearch(){
		keywords_field.value = keywords_field.value === 'keywords' ? '' : keywords_field.value;
		location_field.value = keywords_field.value === 'location' ? '' : location_field.value;
		document.forms[0].submit();	
	}
	
	//wire up video modal
	if(vid !== null){
		vid.addEventListener('click', function(e){
			var media = document.getElementsByTagName("video")[0];;
			e.preventDefault();
			$('#modal').foundation('reveal', 'open', {
				animation: 'fade',
			});
			//do regex to see if on iOS and resize video with px value
			if(Navigator.platform){

			}

		});
	}
	
	//pause the video if the modal is closed
	$(document).on('close.fndtn.reveal', '[data-reveal]', function () {
			media = document.getElementsByTagName("video")[0];
			media.pause();
	});
	
	//wire up search form
	if(search_btn != null){
		search_btn.addEventListener('click', function(e){
			e.preventDefault();
			document.forms[0].submit();
		});	
		keywords_field.addEventListener('keypress', function(e){
			if(e.keyCode === 13){
				submitSearch();
			}
		});	
		location_field.addEventListener('keypress', function(e){
			if(e.keyCode === 13){
				submitSearch();
			}
		});
	}
	
	//wire up placeholder text for forms on IE9
	if(Modernizr.placeholder == false){
		var keywords = document.querySelector('input[name="keywords"]');
		var location = document.querySelector('input[name="loc"]');
		keywords.value = "keywords";
		location.value = "location";
		keywords.addEventListener('focus', function(e){
			if(this.value === "keywords"){
				this.value = "";
			}
		});
		location.addEventListener('focus', function(e){
			if(this.value === "location"){
				this.value = "";
			}
		});
	}
	
	//get jobs for job list page
	if(document.querySelector("#job-list")){
		axcb.GetResults([axcb.HandleResults, axcb.RenderPagination], 30);
		//save url for this search
		localStorage["url"] = window.location.href;
	}
	
	//get details for individual job
	if(document.querySelector("#job-details")){
		var back_link;
		var vid;
		axcb.GetResults([axcb.HandleJobDetailsResults]);
		$("body").on("data-loaded", function(e){
			back_link = document.querySelector("#back-to-serp");
			back_link.href = localStorage["url"];
			vid = document.querySelector("#modal-link");
			if(vid !== null){
				vid.addEventListener('click', function(e){
					e.preventDefault();
					$('#modal').foundation('reveal', 'open', {
						animation: 'fade',
					});
				});
			}
		});
	}
	
	
})(jQuery, window, window.document);