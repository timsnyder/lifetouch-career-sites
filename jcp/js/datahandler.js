/**
 * @fileOverview Makes Ajax calls to Career Builder API and handles results.
 * @author Jesse Raleigh <jesse.raleigh@agent-x.com>
 * @author Tim Snyder <tim@timsnyder.me>
 */

//Variables
var $developer_key = 'WDT90JC6TPRV1C0VBQ4S';
var $talentnetwork_key = 'TN7F4QY6CFF5KCKN7CQW';

var axcb = {};

// Google Integration
axcb.GLOG = false; // enables google analytics event logging
axcb.GACode = 'UA-X49699530-1';
axcb.SiteName = 'careers.jcpenneyportraits.com';

// CareerBuilder API Specific
axcb.DeveloperKey = $developer_key;
axcb.TalentNetworkDID = $talentnetwork_key;
axcb.CompanyDID = 'CHR2546HK3180261JYN';
axcb.TargetDID = 'CHL89T6XNVGSW0V9BMV';
axcb.FlashDID = 'CHM8346VFVDZ6WHXFX1';
axcb.CSHCoBrand = false;

if(typeof search_on !== 'undefined'){
    axcb.SearchAll = typeof search_on.search_all === 'undefined' ? false : true;
}

// Default Field Values
axcb.SearchDefault = '';
axcb.CategoryDefault = 'Select Job Category';
axcb.JobTitleDefault = 'Job Title';
axcb.LocationDefault = 'City, State or Zip';

axcb.TotalPages = 1;
axcb.CurrentPage = 1;
axcb.srch_parms = false;

// Job Details result if applicable
axcb.JobResult = false;

// Search Results
axcb.res = {};
axcb.res.categories = [];
axcb.res.citystates = [];
axcb.res.states = [];
axcb.res.jobs = {};
axcb.res.jobtitles = [];
axcb.res.totalcount = 0;
axcb.coldcache = true;
axcb.jobcache = {
    categories: {},
    citystates: {},
    states: {},
    jobs: {},
    jobtitles: {},
    totalcount: 0,
    totalpages: 0
};
axcb.tv = 1;

// Include google analytics if event logging is enabled
if (axcb.GLOG) {
    // This is almost always used with google analytings
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
        m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', axcb.GACode, axcb.SiteName);
    ga('require', 'displayfeatures');
    ga('send', 'pageview');
    // End google analytics
}


/******************************************
 get jobs from the api
*******************************************/
axcb.GetResults = function(callbacks, per_page, page) {
    per_page = typeof per_page !== 'undefined' ? per_page : 30;
    page = typeof page !== 'undefined' ? page : 1;

    axcb.srch_parms = search_on;

    var parms = {
        'DeveloperKey': axcb.DeveloperKey,
        'IncludePrivateJobs': true,
        'PerPage': per_page,
        'PageNumber': page,
        'UseFacets': true,
        //'CompanyDID': axcb.CompanyDID
        //'TalentNetworkDID': axcb.TalentNetworkDID
    };

    //check params, see if we are searching all lifetouch jobs
    if(axcb.SearchAll){
        parms['CompanyDIDCSV'] = axcb.CompanyDID + "," + axcb.TargetDID + "," + axcb.FlashDID;
    }
    else{
        parms['CompanyDID'] = axcb.CompanyDID;    
    }

    if (axcb.srch_parms !== false) {
        for (var p in axcb.srch_parms) {
            switch (p) {
                case "jobdetails":
                case "HostSite":
                    // do nothing, these cases are to catch Job Details params
                    // which are handled by a separate method
                    break;
                default:
                    parms[p] = axcb.srch_parms[p]; // srch_parms override defaults
                    break;
            }
        }
    }

    axcb.CurrentPage = parms.PageNumber;

    $.get('interfaceGeneric.php', parms, function(data) {
        //console.log(data);
        // Populate job results (will only be 1 val if not actually searching)
        $(data).find('JobSearchResult').each(function() {
            var loc = $.trim($(this).find('Location').text());
            var t = {
                jobTitle: $(this).find('JobTitle').text(),
                empType: $(this).find('EmploymentType').text(),
                url: $(this).find('JobDetailsURL').text(),
                apiurl: $(this).find('JobServiceURL').text().replace(/\&DeveloperKey.*/, ''),
                teaser: $(this).find('DescriptionTeaser').text(),
                postdate: $(this).find('PostedDate').text(),
                did: $(this).find('DID').text(),
                onetCode: $(this).find('OnetCode').text()
            };
            if (axcb.res.jobs[loc] === undefined) {
                axcb.res.jobs[loc] = [];
            }
            if (axcb.res.jobtitles[t.onetCode] === undefined) {
                axcb.res.jobtitles[t.onetCode] = {
                    'title': t.jobTitle,
                    'count': 1
                };
            } else {
                axcb.res.jobtitles[t.onetCode].count++;
            }
            axcb.res.jobs[loc].push(t);
        });

        // Populate the total count
        axcb.res.totalcount = parseInt($.trim($(data).find('TotalCount').text()), 10);
        axcb.TotalPages = parseInt($.trim($(data).find('TotalPages').text()), 10);

        // Run the callbacks
        callbacks.forEach(function(v, k) {
            v.call();
        });
    });
};


/******************************************
Renders job list
Data binding provided by Ractive.js
docs.ractivejs.org/latest/magic-mode
*******************************************/
axcb.HandleResults = function() {
    var joblist = [];
    //View Model
    var vm = new Ractive({
        el: '#job-list-container',
        template: '#template',
        magic: true,
        data: {
            'joblist': joblist
        }
    });

    if (axcb.res.totalcount !== 0) {
        for(var location in axcb.res.jobs){
            if(axcb.res.jobs.hasOwnProperty(location)){
                axcb.res.jobs[location].forEach(function(v, k){
                    axcb.res.jobs[location][k]["loc"] = location;
                    joblist.push(axcb.res.jobs[location][k]);
                });
            }
        }
    }
    console.dir(vm.data);
    $("body").trigger("data-loaded");
};


/******************************************
Individual Job Results rendering
*******************************************/
axcb.HandleJobDetailsResults = function() {
    var parms = {
        'DeveloperKey': axcb.DeveloperKey,
        'did': search_on['did'],
        'jobdetails': true
    };
    
    //View Model
    var vm = {};
    var result = {};
    
    $.get('interfaceGeneric.php', parms, function(data) {
        console.log(data);
        var $job = $(data).find('Job');
        result = {
            'ApplyURL': $job.find('ApplyURL').text(),
            'Categories': $job.find('Categories').text(),
            'CategoriesCodes': $job.find('CategoriesCodes').text(),
            'BeginDate': $job.find('BeginDate').text(),
            'Company': $job.find('Company').text(),
            'CompanyDetailsURL': $job.find('CompanyDetailsURL').text(),
            'CompanyImageURL': $job.find('CompanyImageURL').text(),
            'EmploymentType': $job.find('EmploymentType').text(),
            'ExperienceRequired': $job.find('ExperienceRequired').text(),
            'DegreeRequired': $job.find('DegreeRequired').text(),
            'JobDescription': $job.find('JobDescription').text(),
            'JobRequirements': $job.find('JobRequirements').text(),
            'JobTitle': $job.find('JobTitle').text(),
            'LocationCity': $job.find('LocationCity').text(),
            'LocationState': $job.find('LocationState').text(),
            'LocationCountry': $job.find('LocationCountry').text(),
            'LocationFormatted': $job.find('LocationFormatted').text()
        };

        //clean up result data
        result.LocationCity = $('#snapshotLocation').html(result.LocationCity).text();
        result.Categories = $('#snapshotCategory').html(result.Categories).text();
        result.JobDescription = $('<div/>').html(result.JobDescription).text();
        result.JobRequirements = $('<div/>').html(result.JobRequirements).text();

        vm = new Ractive({
            el: '#job-detail-container',
            template: '#template',
            magic: true,
            data: result
        });
        
        $("body").trigger("data-loaded");
        
    });
};



/******************************************
Generate page links for results view
@todo: move this out to UI layer
*******************************************/
axcb.RenderPagination = function() {
    var url = axcb.SearchAll ? 'lifetouch-jobs.php?' : 'jobs.php?';
    var pager = document.querySelector('#pager');
    var parms = [];
    var jobplural = 'job';
    if (axcb.res.totalcount > 1) {
        jobplural = 'jobs';
    }
    for (var p in axcb.srch_parms) {
        if (p.toLowerCase() != 'pagenumber') {
            parms.push(p + '=' + axcb.srch_parms[p]);
        }
    }
    url += parms.join('&');
    for (var z = 1; z <= axcb.TotalPages; z++) {
        var $lnk = false;
        if (z == axcb.CurrentPage) {
            $lnk = $('<span class="srch_cur_page">' + z + '</span>');
        } 
        else {
            $lnk = $('<a>' + z + '</a>').attr('href', url + '&PageNumber=' + z);
        }
        $(pager).append($lnk);
        $('.job-numbers').text(axcb.res.totalcount + " " + jobplural + " found.");
    }
};



/************************************************
 This is a small helper function to send event logs to google analytics
 event tracking services. It's tied to a global var
 ************************************************/
axcb.elog = function(cat, action, label) {
    if (axcb.GLOG) {
        ga('send', 'event', cat, action, label);
    }
    return true;
};