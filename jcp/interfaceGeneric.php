<?php
class cbapi
{
    private $joburl = 'http://api.careerbuilder.com/v1/jobsearch';
    private $jobdtlurl = 'https://api.careerbuilder.com/v1/job';
    
    //private $developer_key = 'WDHT22G637H7LDSDBZGP';
    public $response = '';
    
    public function __construct() {
        $this->request_handler();
    }
    
    private function request_handler() {
        $params = $this->allparams();
        $myurl = $this->joburl;
        
        // Default behavior is to do a job search
        $urlarr = false;
        $urlstring = "";
        //$jobdetails = false;
        
        foreach ($params as $k => $v) {
            switch (strtolower($k)) {
                case "loc":
                    $v = urlencode($v);
                    $urlarr[] = "location=$v";
                    break;

                case "location":
                    $v = urlencode($v);
                    $urlarr[] = "location=$v";
                    break;

                case "jobdetails":
                    //$jobdetails = true;
                    $myurl = $this->jobdtlurl;
                    break;

                case "mfiles":
                    die(json_encode($this->check_updated($v)));
                    break;

                case "facetcitystate":
                    $v = urlencode($v);
                    $urlarr[] = "$k=$v";
                    break;

                case "excludejobtitles":
                    $v = urlencode($v);
                    $urlarr[] = "$k=$v";
                    break;

                case "facetcompany":
                    $v = urlencode($v);
                    $v = str_replace('+', '%20', $v);
                    $urlarr[] = "$k=$v";
                    break;

                case "keywords":
                    $v = urlencode($v);
                    $urlarr[] = "$k=$v";
                    break;

                default:
                    $urlarr[] = "$k=$v";
                    break;
            }
        }
        if (!is_array($urlarr)) {
            $this->response = $this->get_data($myurl);
        } else {
            $urlstring = implode('&', $urlarr);
            $this->response = $this->get_data($myurl . "?" . $urlstring);
        }
    }
    
    private function check_updated($files) {
        $out = false;
        foreach ($files as $file) {
            $out[$file] = filemtime($file);
        }
        return $out;
    }
    
    public static function param($name) {
        switch ($_SERVER['REQUEST_METHOD']) {
            case "GET":
                if (isset($_GET[$name])) {
                    return $_GET[$name];
                } else {
                    return false;
                }
                break;

            case "POST":
                if (isset($_POST[$name])) {
                    return $_POST[$name];
                } else {
                    return false;
                }
                break;
        }
    }
    
    public static function allparams() {
        if ($_SERVER['REQUEST_METHOD'] == 'GET') {
            return $_GET;
        } else {
            return $_POST;
        }
    }
    
    function get_data($url) {
        //die($url);
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }
}
header('Content-type: text/xml');
$cbapi = new cbapi();
echo $cbapi->response;
