<!doctype html>
<html class="no-js" lang="en">
  <head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/> 
  <title>Lifetouch Portraits - Jobs</title>
  <?php include('inc/header-files.php'); ?>	  			
  </head>
  <body id="job-list">
  
	<header>
	  <div class="constrained">
		<a href="index.php" class="logo">
		  <img src="img/lifetouch-logo.png" alt="Lifetouch Portraits" />
		</a>
		<a href="jobs.php?keywords=&location=" class="back-to-serp">&lt;-- Back to JCPenney Portrait Studio Jobs</a>
	  </div>
	</header>
  
  <div class="main-layout">
	<?php include('inc/header-lifetouch-search.php'); ?>  
	
	<section class="constrained job-list">
		<nav class="row">
			<div class="large-7 medium-7 columns">
				<h2>Current Positions</h2>
			</div>
			<div class="pagination large-9 medium-9 columns">
				<div class="job-numbers">
				</div>
				<div id="pager"></div>
			</div>
		</nav>

		<div id="job-list-container">
			<img src="img/loader.gif" class="ajax-loader" />
			<script id='template' type='text/ractive'>
				{{#joblist}}
				<div class="row job-entry fadeIn animated" data-equalizer>
					<div class="description large-10 medium-10 columns" data-equalizer-watch>
						<a href="lifetouch-job-details.php?did={{did}}">{{jobTitle}}</a>
						<div>{{empType}}</div>
						<p>{{teaser}}</p>
					</div>
					<div class="location large-3 medium-3 columns" data-equalizer-watch>
						<p>{{loc}}</p>
					</div>
					<div class="date-posted large-3 medium-3 columns" data-equalizer-watch>
						<p>{{postdate}}</p>
					</div>
				</div>
				{{else}}
				<h3>No jobs found.</h3>
				{{/joblist}}
			</script>
		</div>

	</section>
	
  </div>
  	
	
	<footer>
	  <div class="constrained">
		<div class="call-to-action-wide">
			<a href="http://www.jobs.net/jobs/lifetouch/en-us/" target="_blank" class="large-btn">Let's Get Connected</a>
			<div class="wrapper">
				<h4>Not Ready To Apply?</h4>
				<p>Join our Talent Network and stay connected!</p>
			</div>
		</div>
	  </div>
	  <div class="footnote">
			<p>&copy; <?php echo date('Y'); ?> Lifetouch Portraits. All Rights Reserved.</p>
		</div>
	</footer>
  
    <script type="text/javascript">
		var search_on = {};
	  <?php
	  foreach($_GET as $k=>$v){
	  	if($v != ''){
	  		echo "search_on[\"$k\"] = '$v';\n";	
	  	}
	  }
	  ?>
  	</script>
  
  <?php include('inc/footer-files.php'); ?>
  
  </body>
</html>