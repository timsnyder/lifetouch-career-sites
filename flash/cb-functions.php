<?php

// Functions for CareerBuilder API
ini_set("allow_url_fopen", "On");

// Variables (currently unused)
$developer_key = 'WDT90JC6TPRV1C0VBQ4S';
$talentnetwork_key = 'TN7F4QY6CFF5KCKN7CQW';

curlUrl('https://api.careerbuilder.com/talentnetwork/config/join/questions/TN7F4QY6CFF5KCKN7CQW/?DeveloperKey=WDT90JC6TPRV1C0VBQ4S', "form");
curlUrl('https://api.careerbuilder.com/tn/JoinForm/Geo/xml?DeveloperKey=WDT90JC6TPRV1C0VBQ4S&TNLanguage=USEnglish', "geo");

// This function is responsible for grabbing the current XML files
// from api.careerbuilder.com and saving them in the ./xml/ folder
//
// Ideally these would be pulled via the api directly when rendering the form, however "allow_url_fopen" was set to "Off" in this environtment
function curlUrl($string, $name) {
    $url = $string;
    $timeout = 10;
    
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $timeout);
    
    try {
        $response = curl_exec($curl);
        curl_close($curl);
        
        // success! Let's parse it and perform whatever action necessary here.
        if ($response !== false) {
            
            /** @var $xml SimpleXMLElement */
            $xml = simplexml_load_string($response);
            $xml->saveXML("xml/" . $name . ".xml");
        } else {
            
            // Warn the user here
            
        }
    }
    catch(Exception $ex) {
        
        // Let user's know that there was a problem
        curl_close($curl);
    }
}

// This function returns a dropdown list based on the Geo request variables
// sent in during the get_join_form() call that renders the form fields
function get_ddl($type) {
    
    $geo = simplexml_load_file('xml/geo.xml');
    $ddl = '<div class="form-field-block">';
    $ddl.= '<select id="' . strtolower($type) . '" name="ddlCountries" required class="requiredField">';
    
    for ($i = 0; $i < count($geo->$type->Value->string) && $i < count($geo->$type->Display->string); ++$i) {
        $value = $geo->$type->Value->string[$i];
        $display = $geo->$type->Display->string[$i];
        
        if ($value == 'us') {
            $selected = ' selected="selected"';
             //marks United States as default
            
        } else {
            $selected = '';
        }
        
        $ddl.= '    <option value="' . $value . '"' . $selected . '>' . $display . '</option>
	';
    }
    
    $ddl.= '</select><span class="required">*</span>';
    $ddl.= '</div>';
    echo $ddl;
}

// Handling the requests coming in through the form Geo variables, with the exception
// of "State" which returned non-US States
function get_geo($formvaluelist) {
    $list = explode('|', $formvaluelist);
    foreach ($list as $item):
        switch ($item) {
            case 'ddlCountries':
                get_ddl('Countries');
                break;

            case 'txtCity':
                echo '<div class="form-field-block">
					<input type="text" name="txtCity" placeholder="City:" required  class="requiredField" /><span class="required">*</span>
				</div>
				';
                break;

            case 'txtPostalCode':
                echo '<div class="form-field-block">
					<input type="text" name="MxDOTalentNetworkMemberInfo_ZipCode" placeholder="Zip/Postal Code:" required class="requiredField" /><span class="required">*</span>
				</div>
				';
                break;
        }
    endforeach;
}

function get_join_form() {
    $fields = simplexml_load_file('xml/form.xml');
    
    foreach ($fields->JoinQuestions->JoinQuestion as $question):
        
        $text = $question->Text;
        $formvalue = $question->FormValue;
        $optionDisplay = $question->OptionDisplayType;
        $required = $question->Required;
        $options = $question->Options->JoinQuestionOption;
        $joinQuestionOption = $question->Options->JoinQuestionOption;
        
        if ($required == "true") {
            $require = 'required class="requiredField"';
            $requirespan = '<span class="required">*</span>';
        } else {
            $require = '';
            $requirespan = '';
        }
        
        if (count($options) > 0) {
            $ddl = '<div class="form-field-block">';
            $ddl.= '<select id="' . $formvalue . '" name="' . $formvalue . '">';
            $ddl.= '<option>' . $text . '</option>
            ';
            
            foreach ($joinQuestionOption as $eachOption) {
                $value = $eachOption->Value;
                $text = $eachOption->DisplayText;
                
                $ddl.= '    <option value="' . $value . '">' . $text . '</option>
                ';
            }
            
            $ddl.= '</select>';
            $ddl.= '</div>';
            echo $ddl;
        } else if ($optionDisplay == 'Geography') {
            echo get_geo($formvalue);
        } else if ($optionDisplay == "ResumeUpload") {
            echo '<div class="form-field-block">
	        	<label class="grey small">Upload a Resume:<br><span>(Microsoft Word or PDF format)</span></label>
	        	<input type="file" name="' . $formvalue . '" />
        	</div>
        	';
        } else {
            echo '<div class="form-field-block">
            	<input type="text" name="' . $formvalue . '" placeholder="' . $text . '" ' . $require . ' />' . $requirespan . '
            </div>
            ';
        }
    endforeach;
}
?>