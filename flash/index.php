<!doctype html>
<html class="no-js" lang="en">
  <head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<title>Flash Portraits - Careers</title>
	<?php include('inc/header-files.php'); ?>
  </head>
  <body id="home-page">
  
	<div id="modal" class="reveal-modal" data-reveal>
		<video controls>
		  <source src="lifetouch.webm" type="video/webm">
		  <source src="lifetouch.mp4" type="video/mp4">
		</video>  
		<a class="close-reveal-modal">×</a>
	</div>
  
	<header>
	  <div class="constrained">
		<a href="index.php" class="logo">
		  <img src="img/logo.png" alt="Flash Portraits" />
		</a>
	  </div>
	</header>
	
	<div class="main-layout">

	  <div class="banner-layout-wrapper">
		  <section class="banner-layout constrained animated fadeIn">
		  	<img src="img/smiling-photographer.png" alt="smiling-photographer" /> 
		  	<div class="site-intro">
			  	<h2 class="animated zoomIn">Picture Yourself Here</h2>
				<p class="animated zoomIn">We will teach you what you need to know about being a professional photographer: how to take stunning portraits while delivering a remarkable experience. Satisfied customers will come back again and again.</p>
		  	</div>
		  </section>
	  </div>
	  
	  <section class="video-layout constrained">
	
		<div class="row">
		  <div class="large-6 medium-6 columns sidebar-wrapper">
			<aside class="tall-sidebar">
				<div class="sidebar-block">
					<h2>Find Jobs</h2>
					<form name="job-search" action="jobs.php" method="get">
						<input type="text" id="keywords" name="keywords" placeholder="keywords" />
						<input type="text" id="location" name="loc" placeholder="location" />
						<a href="#" id="job-search-btn" class="large-btn">Search</a>
					</form>
				</div>
				<div class="sidebar-block">
					<h3>Don't see what you're looking for?</h3>
					<p>Join our Talent Network to receive updates on new job openings if you don't see an opening at a 
					location near you at this time. We look forward to staying connected to you.</p>
					<a href="http://www.jobs.net/jobs/lifetouch/en-us/" target="blank" class="large-btn">Let's Get Connected</a>
				</div>
			</aside>
		  </div>
		  
		  <div class="large-10 medium-10 columns video-large">
		  	<a href="#" id="modal-link" data-reveal-id="modal"><img src="img/video-placeholder.jpg" /></a>
		  </div>
		  
		  <div class="large-10 medium-10 columns">
			<article>
			  <h2>About Us</h2>
				<p>Flash Digital Portraits is operated by Lifetouch Portrait Studios.  Lifetouch Portrait Studios operates over 700 portrait studios, including Flash Digital Portraits.  Our photographers are trained on unique posing, lighting techniques and portrait designs to fit the whole family and tell every customer's unique story.  We specialize in capturing images of newborns, toddlers, siblings and entire families.</p>
			</article>
		  </div>
		</div>
	  </section>
	  
	  <section class="callouts-layout copy">
	  	<div class="constrained">
			<div class="row large-callouts">
			  <div class="large-8 medium-8 columns">
			  	<div class="img-wrapper">
			  		<img src="img/team-members.jpg" />
			  	</div>
				<h2>Studio Team Members</h2>
				<p>Our Team Members become effective sales professionals and skilled photographers. Whether you're looking for part time or seasonal employment (October–December), you’ll have a job you love and some great benefits when you're a part of our team!</p>
				<h3>Studio Team Members enjoy:</h3>
				<ul>
					<li>Paid comprehensive photography and sales training</li>
					<li>Competitive pay and sales perks</li>
					<li>Flexible schedules</li>
					<li>Studio discount</li>
					<li>Participation in our employee stock ownership plan for eligible Studio Team Members</li>
				</ul>
			  </div>
			  <div class="large-8 medium-8 columns">
				<div class="img-wrapper">
				  	<img src="img/camera-close-up.jpg" />
				</div>
				<h2>Studio Managers</h2>
				<p>Our Studio Managers operate and manage a studio. If you'd like to increase your sales effectiveness while you recruit, train and coach members of your team, this is the job for you!</p>
				<h3>Studio Managers enjoy:</h3>
				<ul>
					<li>Paid comprehensive photography and sales training</li>
					<li>Competitive pay and monthly bonus opportunity</li>
					<li>Studio discount</li>
					<li>Medical, dental and life insurance benefits</li>
					<li>Participation in our employee stock ownership plan for eligible Studio Managers</li>
				</ul>
			  </div>
			</div>
			<div class="row small-callouts">
			  <div class="large-16 medium-16 columns">
			  	<h2>Join Our Team If You...</h2>
			  </div>
			  <div class="large-4 medium-4 columns">
			  	<img src="img/portrait01.jpg" class="portrait" />
			  	<p>Value a career that is fun and creative while providing a service and product you can feel good about.</p>	
			  </div>
			  <div class="large-4 medium-4 columns">
				<img src="img/portrait02.jpg" class="portrait" />
				<p>Are motivated to work independently and as a team.</p>
			  </div>
			  <div class="large-4 medium-4 columns">
				<img src="img/portrait03.jpg" class="portrait" />
				<p>Want to be rewarded for your efforts, achievement, and what you put into your career.</p>
			  </div>
			  <div class="large-4 medium-4 columns">
			  	<img src="img/portrait04.jpg" class="portrait" />
				<p>Are high energy, and enjoy interacting with people — children, families and adults alike.</p>
			  </div>
			</div>
	  	</div>
	  </section>
	
	</div>
	
	<footer>
	  <div class="constrained">
		<div class="call-to-action">
			<h2>Apply Today!</h2>
			<a href="jobs.php" class="large-btn">Find Your Job</a>
		</div>
	  </div>
	  <div class="footnote">
			<p>&copy; <?php echo date('Y'); ?> Flash Portraits. All Rights Reserved.</p>
		</div>
	</footer>
	
	<?php include('inc/footer-files.php'); ?>

  </body>
</html>