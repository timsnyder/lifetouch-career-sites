<!doctype html>
<html class="no-js" lang="en">
  <head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>Flash Portraits - Careers</title>
	<?php include('inc/header-files.php'); ?>
  </head>
  <body id="home-page">
  
	<header>
	  <div class="constrained">
		<a href="index.php" class="logo">
		  <img src="img/logo.png" alt="Flash Portraits" />
		</a>
	  </div>
	</header>
	
	<div class="main-layout">
	  
	  <div class="banner-layout-wrapper">
		  <section class="banner-layout constrained animated fadeIn">
		  	<img src="img/smiling-photographer.png" alt="smiling-photographer" /> 
		  	<div class="site-intro">
			  	<h2 class="animated zoomIn">Picture Yourself Here</h2>
				<p class="animated zoomIn">We will teach you what you need to know about being a professional photographer: how to take stunning portraits while delivering a remarkable experience. Satisfied customers will come back again and again.</p>
		  	</div>
		  </section>
	  </div>
	  
	  <section class="video-layout constrained">
		<video controls>
		  <source src="lifetouch.webm" type="video/webm">
		  <source src="lifetouch.mp4" type="video/mp4">
		</video>  
	  </section>
	  
	  <section class="callouts-layout copy">
	  </section>
	
	</div>
	
	<footer>
	  <div class="constrained">
		<div class="call-to-action">
			<h2>Apply Today!</h2>
			<a href="jobs.php" class="large-btn">Find Your Job</a>
		</div>
	  </div>
	  <div class="footnote">
			<p>&copy; <?php echo date('Y'); ?> Flash Portraits. All Rights Reserved.</p>
		</div>
	</footer>
	
	<?php include('inc/footer-files.php'); ?>

  </body>
</html>