    <aside class="header-search">
      <div class="constrained">
          <form name="job-search" action="lifetouch-jobs.php" method="get">
            <div class="search-fields">
	            <input type="text" id="keywords" name="keywords" placeholder="keywords" />
	            <input type="text" id="location" name="loc" placeholder="location" />
              <input type="hidden" id="search-all" name="search_all" value="true" />
            </div>
            <a href="#" id="job-search-btn" class="large-btn">Search</a>
          </form>
      </div>
    </aside>