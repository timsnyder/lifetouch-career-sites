    <aside class="header-search">
      <div class="constrained">
          <form name="job-search" action="jobs.php" method="get">
            <div class="search-fields">
	            <input type="text" id="keywords" name="keywords" placeholder="keywords" />
	            <input type="text" id="location" name="loc" placeholder="location" />
            </div>
            <a href="#" id="job-search-btn" class="large-btn">Search</a>
          </form>
      </div>
    </aside>