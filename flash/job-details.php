<!doctype html>
<html class="no-js" lang="en">
	<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/> 
	<title>Flash Portraits - Job Details</title>
	<?php include('inc/header-files.php'); ?>
	</head>
	<body id="job-details">
	
  	<div id="modal" class="reveal-modal" data-reveal>
		<video controls>
		  <source src="lifetouch.webm" type="video/webm">
		  <source src="lifetouch.mp4" type="video/mp4">
		</video>  
		<a class="close-reveal-modal">×</a>
	</div>
	
	<header>
	  <div class="constrained">
		<a href="index.php" class="logo">
		  <img src="img/logo.png" alt="Flash Portraits" />
		</a>
	  </div>
	</header>
	
	<div class="main-layout">
	
		<?php include('inc/header-search.php'); ?>  
		
			<section id="job-detail-container" class="constrained job-details">
				
				<img src="img/loader.gif" class="ajax-loader" />

				<script id='template' type='text/ractive'>
				
					<div class="row fadeIn animated">
						
						<div class="large-11 medium-11 columns">
						
								<article>
									
									<header>
										<h2>{{JobTitle}}</h2>
										<h4>{{LocationFormatted}}</h4>
										<div class="job-snapshot">
											<h3>Job Snapshot</h3>
											<div>
												<ul>
													<li>Employee Type:</li>
													<li>Location:</li>
													<li>Experience:</li>
													<li>Date Posted:</li>
												</ul>
											</div>
											<div>
												<ul>
													<li>{{EmploymentType}}&nbsp;</li>
													<li>{{LocationFormatted}}&nbsp;</li>
													<li>{{ExperienceRequired}}&nbsp;</li>
													<li>{{BeginDate}}</li>
												</ul>
											</div>
										</div>
									</header>
									
									<div class="copy">
										<h3>Job Description</h3>
										<div>{{{JobDescription}}}</div>
										
										<h3>Job Requirements</h3>
										<div>{{{JobRequirements}}}</div>
										
									</div>
									
								</article>
						
						</div>
						
						<div class="large-5 medium-5 columns sidebar-wrapper">
							
							<aside>
								<div class="sidebar-callouts">
									<a href="{{{ApplyURL}}}" class="large-btn">Apply Now</a>	
									<a href="#" id="back-to-serp" class="back-to-serp">&lt;-- Back to search results</a>
									<div class="video">
										<a href="#" id="modal-link" data-reveal-id="modal"><img src="img/video-placeholder.jpg" /></a>
									</div>
								</div>
								
								<div class="short-sidebar">
									<div class="sidebar-block">
										<h3>Don't see what you're looking for?</h3>
										<p>Join our Talent Network to recieve updates on new job openings if you don't see an opening at
										a location near you at this time. We look forward to staying connected with you.</p>
										<a href="http://www.jobs.net/jobs/lifetouch/en-us/" target="_blank" class="large-btn">Let's Get Connected</a>	
									</div>
								</div>
							</aside>
							
						</div>
					</div>  
			
				</script>
				
			</section>
		
	</div>
	
	<footer>
	  <div class="constrained">
		<div class="call-to-action-wide">
			<a href="http://www.jobs.net/jobs/lifetouch/en-us/" target="_blank" class="large-btn">Let's Get Connected</a>
			<div class="wrapper">
				<h4>Not Ready To Apply?</h4>
				<p>Join our Talent Network and stay connected!</p>
			</div>
		</div>
	  </div>
	  <div class="footnote">
			<p>&copy; <?php echo date('Y'); ?> Flash Portraits. All Rights Reserved.</p>
		</div>
	</footer>
	<script type="text/javascript">
		var search_on = {};
		<?php
			foreach($_GET as $k=>$v){
				if($v != ''){
					echo "search_on[\"$k\"] = '$v';\n";	
				}
			}
		?>
	</script>
	
	<?php include('inc/footer-files.php'); ?>
	
	</body>
</html>